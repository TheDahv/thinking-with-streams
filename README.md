# Learn Streams with Highland

A Jupyter notebook with iJavaScript to teach streaming concepts using
the Highland utility library to solve problems familiar to JS
programmers.

Make sure Docker is installed and running on your system.

Then run `./build-and-run.sh` and open your browser to the URL the
Jupyter server provides in its console output.

Credit to [@EvilScott](https://github.com/EvilScott) for figuring out how to
Dockerize a JavaScript Jupyter kernel:
[https://hub.docker.com/r/evilscott/nodebook/](https://hub.docker.com/r/evilscott/nodebook/)
