{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Now you're thinking with streams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The concept of streaming data is incredibly powerful and prevalent in computing. As far as Linux and Unix are concerned, everything is a file--devices, network connections, log files, etc.--and files are streams of data.\n",
    "\n",
    "Modeling your problem as a stream is great when any of these are true:\n",
    "\n",
    "- you can't fit your problem into memory at once\n",
    "- you can compose small, modular, functional code to solve your problem\n",
    "- you can deal with data in pieces rather than all at once\n",
    "- your problem domain can be modeled as occurring over time\n",
    "- you just want to because it is fun\n",
    "\n",
    "JavaScript has great support for modeling problems as streams as well.\n",
    "\n",
    "And any time I work with streams, I reach for [highland.js](http://highlandjs.org/).\n",
    "\n",
    "This guide is a simple way to look at problems you've probably solved before, and see how we might approach them as a streams processing problem.\n",
    "\n",
    "So, first things first: enter Highland"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "const highland = require('highland');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Woo hoo! Easy enough.\n",
    "\n",
    "Now, we've brought this in as a Node module, but this could just as easily run in the browser.\n",
    "\n",
    "The project compiles a browser ready version for you to use:\n",
    "\n",
    "> Highland can be used both in Node.js and in the browser. When you install the highland package, you will find a dist/highland.js file in the package hierarchy. This file has been prepared with browserify in order to bring a browser-ready version of highland.\n",
    "\n",
    "Alternatively, you can also find it on [cdnjs.com](https://cdnjs.com/libraries/highland).\n",
    "\n",
    "Moving on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's think about numbers. More specifically, let's think about a list of numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const nums = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We already have functions we can use to work with arrays (map, filter, etc.). We can also think of an array as a stream of values and work with them that way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// First we wrap the list of numbers in Highland.\n",
    "const stream = highland(nums);\n",
    "// Now it's a stream! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Node already ships with a [Streams processing API](https://nodejs.org/api/stream.html). It's primarily event-based, and you can write code to hook into events. \n",
    "\n",
    "However, a unique aspect of a stream is that it works in \"chunks\". \n",
    "\n",
    "See, Streams are not a panacea for dealing with large amounts of data. They are a strategy of dealing with the data in pieces as time so you don't overwhelm your processing resources.\n",
    "\n",
    "This helps provide something called \"backpressure\" which basically means a _producer_ of a stream knows when to slow down if the _consumer_ of the stream can't keep up.\n",
    "\n",
    "So, you could work with streams on your own. But then you have to buffer up chunks until you have a minimally-sized unit of work to process. And you have to manage backpressure. And you have to do all these things.\n",
    "\n",
    "They're great, but it doesn't harmonize as well with other API's and other higher-order functions we expect out of a functional programming library.\n",
    "\n",
    "So, now that we have a stream of numbers, rather than working with code like:\n",
    "\n",
    "    stream.on('data', (num) => console.log(num));\n",
    "    \n",
    "We can chain functions to describe a data processing pipeline. So let's try that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const onlyEvens = num => num % 2 === 0;\n",
    "const double = num => num * 2;\n",
    "const square = num => num ** 2;\n",
    "\n",
    "stream.\n",
    "  filter(onlyEvens).\n",
    "  map(double).\n",
    "  map(square);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ok, what the heck. That didn't _do_ anything.\n",
    "\n",
    "Here's why. Streams must be _consumed_. They're very lazy.\n",
    "\n",
    "So far, all we have set up is a _description_ of what our data processing library will do once data flows through it.\n",
    "\n",
    "So let's set up a new stream and actually spit out some numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "highland(nums).\n",
    "  filter(onlyEvens).\n",
    "  map(double).\n",
    "  map(square).\n",
    "  each(num => console.log(num))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More Interesting Stream Sources\n",
    "\n",
    "Ok, so we can make a stream that processes numbers. But this isn't that interesting. It's a static piece of data with a known size and end. We could make it a bigger array, but that's still just a fancy version of a for loop.\n",
    "\n",
    "What about data that is more event-based or asynchronous in nature.\n",
    "\n",
    "We also have the idea of generators in JavaScript which can act as a \"pump of data\" that can be consumed as a stream of data. So let's set up a generator builder."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const makeGen = max => function* nums() {\n",
    "    let state = 1;\n",
    "    while (state <= max) {\n",
    "        yield state;\n",
    "        state++;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can make generators that produce numbers when they are run and passed to a Highland stream.\n",
    "Let's make 100 numbers and run them through our favorite whiteboard problem: Fizzbuzz."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const hundredNums = makeGen(100);\n",
    "\n",
    "const fizzbuzz = (num) => {\n",
    "    if (num % 3 === 0) {\n",
    "        return 'fizz';\n",
    "    } else if (num % 5 === 0) {\n",
    "        return 'buzz';\n",
    "    } else if (num % 15 === 0) {\n",
    "        return 'fizzbuzz';\n",
    "    }\n",
    "    return num;\n",
    "}\n",
    "\n",
    "highland(hundredNums()).\n",
    "  map(fizzbuzz).\n",
    "  each(console.log)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More Interesting Data\n",
    "\n",
    "Ok, these are just simple numbers--not complex objects. These are just values we have right now, not things we have to wait for.\n",
    "\n",
    "Let's try something more fun:\n",
    "\n",
    "- Parsing a CSV\n",
    "- The CSV lives on the Internet (or in our case, a file on disk from the Internet, because we don't get browser functions)\n",
    "- The CSV streams in as a file\n",
    "- The CSV has presidents in it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const fs = require('fs');\n",
    "// We downloaded this from \n",
    "const presidentsPath = 'presidents.csv';\n",
    "\n",
    "// We can probably write something fancy to parse the header\n",
    "// row and transform CSV rows into objects, but we're going to do\n",
    "// it the easy way for now:\n",
    "const headers = [\n",
    "    'Presidency',\n",
    "    'President',\n",
    "    'Wikipedia Entry',\n",
    "    'Took office',\n",
    "    'Left office',\n",
    "    'Party',\n",
    "    'Portrait',\n",
    "    'Thumbnail',\n",
    "    'Home State'\n",
    "];\n",
    "\n",
    "const contents = fs.createReadStream(presidentsPath);\n",
    "\n",
    "// Ok, so we don't actually have data yet. We have a Promise for the data.\n",
    "// And possibly a giant error. Can we handle that?\n",
    "\n",
    "// Check out what Highland says about its constructor: http://highlandjs.org/#_(source)\n",
    "// We can wrap Promises and treat them as a stream of data\n",
    "\n",
    "highland(contents).\n",
    "  // Ok, so we have a stream of file contents. We mentioned earlier\n",
    "  // files are streamed in chunks. Let's organize our chunks into lines\n",
    "  split('\\n').\n",
    "  // We already have our headers, so let's drop the first line\n",
    "  drop(1).\n",
    "  // Let's turn each line into an array of fields. Normally we'd use 'map'\n",
    "  // for that, but this is super common. Highland already has a function for that\n",
    "  invoke('split', [',']).\n",
    "  // Now we can turn our rows into objects\n",
    "  map(row => {\n",
    "    return headers.reduce(\n",
    "        (president, header, idx) => Object.assign(president, { [header]: row[idx] }),\n",
    "        {}\n",
    "    );\n",
    "  }).\n",
    "  // We can use 'tap' to transparently hook into our stream and inspect \n",
    "  // contents at any point. Let's see what kind of objects we got\n",
    "  tap(console.log).\n",
    "  // Let's calculate the length of years in office for each president\n",
    "  // This file was written when Obama was still in office, so we need to drop\n",
    "  // entries that don't have valid start or end dates\n",
    "  reject(row => /Incumbent/.test(row['Left office'])).\n",
    "  map(row => {\n",
    "    const parseDate = (date = '') => {\n",
    "        const [ dd, mm, yyyy ] = date.split('/').map(part => parseInt(part, 10));\n",
    "        return new Date(yyyy, mm - 1, dd);        \n",
    "    }\n",
    "    const start = parseDate(row['Took office']);\n",
    "    const end = parseDate(row['Left office']);\n",
    "    \n",
    "    // Rough calculation by going from milliseconds to years\n",
    "    return Object.assign(\n",
    "        row,\n",
    "        { yearsInOffice: (end - start) / 1000 / 60 / 60 / 24 / 365 }\n",
    "    );\n",
    "  }).\n",
    "  tap(console.log).\n",
    "  // If we wanted to compute the average tenure, we could reach for reduce,\n",
    "  // but we don't know the total number of values as we're processing the\n",
    "  // stream. So now we must consume the entire stream to produce our computation\n",
    "  pluck('yearsInOffice').\n",
    "  toArray(years => {\n",
    "    console.log({\n",
    "        averageYears: years.reduce((total, num) => total + num, 0) / years.length\n",
    "    })\n",
    "  })"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Asynchronous Transformations\n",
    "\n",
    "Ok, we can handle data that comes to us asynchronously. What should we do if our stream transformation functions are asynchronous?\n",
    "\n",
    "For example, let's say I have a list of product IDs, and I need to fetch each one. We could asynchronously grab them all _before_ passing them into the stream. But what if we have downstream dependencies that are also asynchronous--say maybe we need to extract a field from a result and then use that for a database load or another HTTP fetch.\n",
    "\n",
    "We need our stream to also be asynchronous. Highland can do that.\n",
    "\n",
    "The key is to think of asynchronous processes as streams themselves. Given 10 IDs and 10 resulting asynchronous operations, mapping each ID to a asynchronous operation should result in 10 streams.\n",
    "\n",
    "We can \"flatten\" out those streams into a single stream and resume processing as if it were all in one flow of data.\n",
    "\n",
    "We have at least 2 tools at our disposals for handling asynchronous results:\n",
    "\n",
    "- Callbacks\n",
    "- Promises"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const names = [\n",
    "    'Archie',\n",
    "    'Betty',\n",
    "    'Cheryl',\n",
    "    'Chuck',\n",
    "    'Dilton',\n",
    "    'Ethel',\n",
    "    'Hiram',\n",
    "    'Jason',\n",
    "    'Jughead',\n",
    "    'Kevin',\n",
    "    'Midge',\n",
    "    'Moose',\n",
    "    'Nancy',\n",
    "    'Reggie',\n",
    "    'Veronica'\n",
    "];\n",
    "\n",
    "// Let's look at callbacks first:\n",
    "highland(names).\n",
    "  // flatMap creates a new Stream of values by applying each item in a\n",
    "  // Stream to an iterator function which must return a (possibly \n",
    "  // empty) Stream. Each item on these result Streams are then emitted\n",
    "  // on a single output Stream.\n",
    "  map(\n",
    "    // wrapCallback takes a Node-style function that takes a value and a\n",
    "    // callback to call to pass the operation through, and then returns a\n",
    "    // stream object for the result of that function. As is true in node,\n",
    "    // the first argument to the callback is an error if it exists, and null\n",
    "    // otherwise. We haven't discussed errors yet, but know that the Highland\n",
    "    // API has ways to handle those too.\n",
    "    highland.wrapCallback((name, callback) => {\n",
    "        setTimeout(\n",
    "            callback.bind(null, null, name.toUpperCase()), \n",
    "            500\n",
    "        );\n",
    "    })\n",
    "  ).\n",
    "  // Recall that the map function produces Streams, not the actual values.\n",
    "  // The sequence helper arranges for these Streams to produce their values\n",
    "  // and merge their values into a single stream.\n",
    "  sequence().\n",
    "  each(console.log);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// We can do the same thing with promises. This is also going to use a\n",
    "// simplified version of 'map().sequence()' called 'flatMap' which does\n",
    "// the same thing. Also check out 'flatFilter' and 'merge' which work\n",
    "// streams of streams.\n",
    "highland(names).\n",
    "  flatMap(name => {\n",
    "    const p = new Promise(resolve => {\n",
    "        setTimeout(\n",
    "            resolve.bind(null, name.toUpperCase()),\n",
    "            500\n",
    "        )\n",
    "    });\n",
    "    \n",
    "    // The one wringle here is Highland expects to deal with\n",
    "    // values or Highland streams. We have a Promise, which\n",
    "    // isn't either one. But we *can* wrap it with the Highland\n",
    "    // constructor to produce a Stream that will emit its value\n",
    "    // when the Promise resolves\n",
    "    return highland(p);\n",
    "  }).\n",
    "  each(console.log)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stream Composition and Library Interop\n",
    "\n",
    "Highland isn't the only thing that talks in streams. As we saw earlier, Node's `fs` library treats many file system IO operations as streams. It's request and response objects from the `http` library are also readable and writable streams, respectively.\n",
    "\n",
    "We can also \"connect\" streams with the \"pipe\" operator. Think about how we do this in Unix/Linux:\n",
    "\n",
    "    ls -l | grep -v total | awk '{print $3}' | sort | uniq -c\n",
    "    \n",
    "We used pipes to connect the `stdout` stream from one command to the `stdin` stream of the subsequent.\n",
    "\n",
    "If a library has a Streams API, you can use it with Highland to do the same thing.\n",
    "\n",
    "Let's reconsider our presidents example from before. We had to hand-roll our CSV parsing, and we didn't cover all the corner cases and idiosyncracies in CSV formats. \n",
    "\n",
    "Our job is to write contrived demos, not write a CSV parser!\n",
    "\n",
    "So let's see how we can use the wonderful [node-csv](https://github.com/adaltas/node-csv) package to solve this problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const fs = require('fs');\n",
    "const Parser = require('csv-parse/lib/es5');\n",
    "\n",
    "// Create a writable stream we can send data to\n",
    "// It also emits parsed rows\n",
    "const parser = Parser({ columns: true });\n",
    "\n",
    "// This creates a new stream of parsed rows. Notice how we used\n",
    "// pipe and we haven't used Highland at all to do this.\n",
    "const entries = fs.createReadStream('presidents.csv').pipe(parser);\n",
    "\n",
    "// To bring this string back into Highland, we can wrap it to put\n",
    "// our familiar API on top of it\n",
    "const names = highland(entries).pluck('President ');\n",
    "\n",
    "// We could continue messing with it from here, but lets pretend\n",
    "// we want to send this stream to another destination. We still have\n",
    "// 'pipe' as part of our API on the stream pipeline. We'll use Node's\n",
    "// 'process.stdout' stream to act as a destination stream, but you could\n",
    "// write to a file, to a database connection, or a Node HTTP response\n",
    "// object\n",
    "names.intersperse('\\n').pipe(process.stdout)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Backpressure, Forking, and Observing\n",
    "\n",
    "Ok, first things first [read this section](http://highlandjs.org/#backpressure) from the Highland documentation.\n",
    "\n",
    "The big idea is your stream processors communicate back upstream about how fast they are processing work and whether the stream producers should pause or throttle to prevent backups.\n",
    "\n",
    "You can also split up your streams to treat various data differently. For example, you may decide \"apples\" go in one file, and \"oranges\" go in another. One way to accomplish that would be to \"fork\" the stream into two new streams, apply the appropriate filters on each stream, and process them accordingly.\n",
    "\n",
    "However, to pull this off, you need to make a decision about how the various forked streams should communicate back to the source. If there are three streams and one of them is a slow consumer, should the other 2 wait for the slow consumer to finish before the source emits a new value? Are they ok to run independently?\n",
    "\n",
    "Ultimately, this is up to you, your program goals, and what resources (namely, memory) you have at your disposal.\n",
    "\n",
    "Let's see an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Shared back pressure\n",
    "const fiftyNums = makeGen(50);\n",
    "const nums = highland(fiftyNums());\n",
    "\n",
    "// Here we set up two streams that divide up the source\n",
    "// stream into 2 independent channels of data\n",
    "const evens = nums.fork().filter(num => num % 2 === 0);\n",
    "const odds  = nums.fork().reject(num => num % 2 === 0);\n",
    "\n",
    "// Now we set up the consumption of the even stream. Notice that each\n",
    "// 'map' takes 500 milliseconds before emitting a value back into the\n",
    "// stream. This \"500 milliseconds before I'm ready for another value\"\n",
    "// is back pressure.\n",
    "evens.\n",
    "    map(num => new Promise(resolve => setTimeout(resolve.bind(null, num), 500))).\n",
    "    flatMap(highland).\n",
    "    each(console.log)\n",
    "\n",
    "// Notice that the odds stream only takes 100 milliseconds before \n",
    "// it is ready for another value. It is ready to take on new work\n",
    "// in 1/5 of the time it takes for evens to get through its pipeline.\n",
    "// However, these streams are forked and share back-pressure.\n",
    "odds.\n",
    "    map(num => new Promise(resolve => setTimeout(resolve.bind(null, num), 100))).\n",
    "    flatMap(highland).\n",
    "    each(console.log);\n",
    "\n",
    "// Pay specific attention to the order and at what speed the results\n",
    "// are emitted when running this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Independent back pressure\n",
    "const fiftyNums = makeGen(50);\n",
    "const nums = highland(fiftyNums());\n",
    "\n",
    "// Here we set up the same two streams. Notice one of them is\n",
    "// forked and the other observed. The observed stream does not\n",
    "// share back-pressure with its siblings. We need at least\n",
    "// one stream forked to communicate progress back to the source\n",
    "// stream.\n",
    "const evens = nums.observe().filter(num => num % 2 === 0);\n",
    "const odds  = nums.fork().reject(num => num % 2 === 0);\n",
    "\n",
    "evens.\n",
    "    map(num => new Promise(resolve => setTimeout(resolve.bind(null, num), 500))).\n",
    "    flatMap(highland).\n",
    "    each(console.log)\n",
    "odds.\n",
    "    map(num => new Promise(resolve => setTimeout(resolve.bind(null, num), 100))).\n",
    "    flatMap(highland).\n",
    "    each(console.log);\n",
    "\n",
    "// Run this and see what order the events come in at. Which finishes first?\n",
    "// Then swap out the fork and observe calls and see how the behavior changes.\n",
    "// What does this tell you about how you split your streams?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "There is a lot more to Highland that we didn't cover. \n",
    "\n",
    "There are tons of great utility functions that support high-level functional programming like:\n",
    "\n",
    "- [`curry`](http://highlandjs.org/#curry) / [`partial`](http://highlandjs.org/#partial)\n",
    "- [`compose`](http://highlandjs.org/#compose) / [`seq`](http://highlandjs.org/#seq)\n",
    "- [`extend`](http://highlandjs.org/#extend) / [`pluck`](http://highlandjs.org/#pluck)\n",
    "\n",
    "You can also control the flow and resource utilization through the stream with:\n",
    "\n",
    "- [`batch`](http://highlandjs.org/#batch)\n",
    "- [`batchWithTimeOrCount`](http://highlandjs.org/#batchWithTimeOrCount)\n",
    "- [`debounce`](http://highlandjs.org/#debounce)\n",
    "- [`ratelimit`](http://highlandjs.org/#ratelimit)\n",
    "- [`throttle`](http://highlandjs.org/#throttle)\n",
    "\n",
    "\n",
    "We also didn't cover error handling, but you can decide when and where to handle errors at any point in your stream with [`errors`](http://highlandjs.org/#errors).\n",
    "\n",
    "The functions and examples we covered were mostly high-level stream consumption and transformation problems. Highland can also model writable streams that receive data from another source and process them accordingly.\n",
    "\n",
    "If you've taken the time to explore utility libraries like [Underscore.js](http://underscorejs.org/), [Lodash](https://lodash.com/), and [Async](https://caolan.github.io/async/), Highland will fit right in and you'll get more out of it the more you explore.\n",
    "\n",
    "Now you're thinking with Streams! Have fun!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jp-Babel (Node.js)",
   "language": "babel",
   "name": "babel"
  },
  "language_info": {
   "file_extension": ".js",
   "mimetype": "application/javascript",
   "name": "javascript",
   "version": "4.2.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
